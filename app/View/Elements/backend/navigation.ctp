<div id="left" >
	<ul id="menu" class="collapse">
		
		<li class="panel active">
			<?php
				echo $this->Html->link(
					'<i class="icon-home"></i> '.__('Dashboard'),
					'/admin/admins/dashboard',
					array('escape' => FALSE)
				);
			?>
		</li>
		<?php if ($this->Session->read('Auth.User.user_type_id') == configure::read('UserTypes.Admin')) { ?>
			<li class="panel active">
				<?php
					echo $this->Html->link(
							'<i class="icon-user"></i> '.__('Manage Administrators'),
							'/admin/admins/',
							array(
									'escape' => false
								)
						);
				?>
			</li>
		<?php } ?>
		<li class="panel active">
			<?php
				echo $this->Html->link(
						'<i class="icon-user"></i> '.__('Users'),
						'/admin/admins/userListing',
						array(
								'escape' => false
							)
					);
			?>
		</li>
	   <li class="panel active">
			<?php
				echo $this->Html->link(
						'<i class="icon-envelope"></i> '.__('Manage Email Templates'),
						'/admin/email_templates/',
						array(
								'escape' => false
							)
					);
			?>
		</li>
        <li class="panel active">
			<?php
				echo $this->Html->link(
						'<i class="icon-cogs"></i> '.__('Manage Types'),
						'/admin/Types/',
						array(
								'escape' => false
							)
					);
			?>
		</li>
		<li class="panel">
            <?php
                echo $this->Html->link(
                        '<i class="icon-wrench"></i> '.__('Manage Settings').'
                        <span class="pull-right">
                          <i class="icon-angle-right"></i>
                        </span>
                        ',
                        '#',
                        array(
                            'data-parent' => '#menu',
                            'data-toggle' => 'collapse',
                            'class' => 'accordion-toggle',
                            'data-target' => '#component-nav',
                            'escape' => false
                            )
                    );
                $classCollapseIn = $this->Admin->manageSettingsTabCollapsing();
            ?>
            <ul class="<?php echo $classCollapseIn; ?>" id="component-nav">
                <li>
                    <?php
                        $tabIconClass = $this->Admin->manageSettingsInnerTabCollapsing('admin_listCmsPages');
                        echo $this->Html->link('<i class="icon-angle-right '.$tabIconClass.'"></i> '.__('List CMS pages'),
                            '/admin/AdminSettings/listCmsPages',
                            array(
                                'escape' => false
                                )
                        );
                    ?>
                </li>
            </ul>
        </li>
	</ul>
</div>
