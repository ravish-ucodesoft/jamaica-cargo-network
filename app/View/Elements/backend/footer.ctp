<p><?php echo "Copyright &copy; Jamaica Cargo Network Inc. 2015" ?></p>
<?php
    echo $this->Html->script(array(
        'backend/Admin/edit_admin_profile',
        'backend/commonmagnificpopup',
        ),
        array('block' => 'scriptBottom')
    );
?>

<?php
    echo $this->element('backend/delete_popup');
    echo $this->element('backend/restore_popup');
    echo $this->element('backend/active_popup');
    echo $this->element('backend/inactive_popup');
?>