<div class="modal fade" id="addTypeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <span class="button b-close class="close" data-dismiss="modal" aria-label="Close"">
                     <span>X</span>
                </span>
                <h4 class="modal-title" id="myModalLabel"><?php echo __('Add New Type'); ?></h4>
            </div>
            <div class="modal-body">
                <?php echo $this->Form->create('Type', array(
                                'url' => '/admin/Types/addType',
                                'method' => 'post',
                                'id' => 'add-type',
                                'novalidate' => true
                                ));
                ?>
                    <div class="form-group">
                        <label class="control-label"><?php echo __('Type Name'); ?><span>*</span></label>
                        <?php
                            echo $this->Form->input('name', array(
                                        'class' => 'form-control',
                                        'label' => false,
                                        'div' => false,
                                        'placeholder' => 'Type Name'
                                        ));
                        ?>
                    </div>
                	<div class="form-group">
                        <div class="clearfix">                               
                            <div class="pull-right">
                                <?php
                                    echo $this->Form->button(__('Close'), array(
                                                'class' => 'btn btn-default',
                                                'data-dismiss' => 'modal'
                                                ));
                                    echo $this->Form->button(__('Save'), array(
                                                'class' => 'btn btn-primary btn-grad btn-left',
                                                'type' => 'submit'
                                                ));
                                ?>
                            </div>
                        </div> 
                    </div>     
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>
