<?php
    echo $this->element('backend/pagination_options');
?>
<table class="table table-bordered table-striped table-bordered table-hover new-addministartor-table">
    <thead>
        <tr>
            <th>
                <?php echo $this->Paginator->sort('Type.name', 'Type Name', array('class' => 'tableinherit')); ?>
            </th>
            <th>
                <?php echo $this->Paginator->sort('Type.created', 'Created', array('class' => 'tableinherit')); ?>
            </th>
            <th><?php echo __('Status'); ?></th>
            <th><?php echo __('Actions'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php
            if(!empty($types)) { 

                foreach ($types as $showTypes) {
        ?>
                    <tr>
                        <td>
                            <?php echo $showTypes['Type']['name']; ?>
                        </td>
                        <td>
                            <?php echo $showTypes['Type']['created']; ?>
                        </td>
                        <td>
                        	<?php if ($showTypes['Type']['is_activated'] == 0) { ?>
                                <a class="btn btn-danger btn-xs btn-grad" href="#"><?php echo __('Inactive'); ?></a>
                            <?php } else { ?>
                            	<a class="btn btn-success btn-xs btn-grad" href="#"><?php echo __('Active'); ?></a>
                            <?php } ?>
                        </td>
                        <td id="inline-popups">
                            <?php
                                if ($showTypes['Type']['is_activated'] == 1) {
    								echo $this->Html->link(
                                            '<i class="glyphicon glyphicon-remove"></i>',
                                            '#inactiveConfirm',
                                            array(
                                                'class' => 'btn btn-ok activeinactivemodalbutton magniPopup',
                                                'title' => 'Inactivate',
                                                'data-effect' => 'mfp-zoom-in',
                                                'escape' => false,
                                                'data-url' => '/admin/admins/changeRecordStatus/'.base64_encode($showTypes['Type']['id']).'/Type'
                                            )
                                        );
                                } else {
                                    echo $this->Html->link(
                                            '<i class="glyphicon glyphicon-ok"></i>',
                                            '#activeConfirm',
                                            array(
                                                'class' => 'btn btn-ok activeinactivemodalbutton magniPopup',
                                                'title' => 'Activate',
                                                'data-effect' => 'mfp-zoom-in',
                                                'escape' => false,
                                                'data-url' => '/admin/admins/changeRecordStatus/'.base64_encode($showTypes['Type']['id']).'/Type'
                                            )
                                        );
                                }
                            ?>
                            <?php
                                echo $this->Html->link(
                                        '<i class="glyphicon glyphicon-pencil"></i>',
                                        '#',
                                        array(
                                            'class' => 'btn btn-green edit_type_data',
                                            'data-effect' => 'mfp-zoom-in',
                                            'data-toggle' => 'modal',
                                            'data-target' => '#editTypeModal',
                                            'title' => 'Edit',
                                            'data-url' => '/admin/Types/ajaxGetTypeData/'.base64_encode($showTypes['Type']['id']).'.json',
                                            'escape' => false
                                        )
                                    );
                            ?>
                            <?php
                                $deleteRestoreAttr = $this->Admin->deleteRestoreIconValues($showTypes['Type']['is_deleted']);
                                echo $this->Html->link(
                                        '<i class="glyphicon glyphicon-'.$deleteRestoreAttr['Class'].'"></i>',
                                        $deleteRestoreAttr['PopupID'],
                                        array(
                                            'class' => 'btn btn-red deleteModalButton magniPopup',
                                            'title' => $deleteRestoreAttr['Title'],
                                            'data-effect' => 'mfp-zoom-in',
                                            'data-url' => '/admin/admins/delete/'.base64_encode($showTypes['Type']['id']).'/Type',
                                            'escape' => false
                                        )
                                    );
                            ?>
                        </td>
                    </tr>
        <?php } } else { ?>
            <tr>
                <td colspan="8">
                    <?php echo __('No Record Found'); ?>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<?php
    echo $this->element('backend/pagination');
    echo $this->Js->writeBuffer();
?>