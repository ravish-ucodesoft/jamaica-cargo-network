<div class="modal fade" id="editTypeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <span class="button b-close class="close" data-dismiss="modal" aria-label="Close"">
                     <span>X</span>
                </span>
                <h4 class="modal-title" id="myModalLabel">Edit Type</h4>
            </div>
            <div class="modal-body">
                <?php echo $this->Form->create('Type', array(
                                'url' => array(
                                        'controller' => 'Types',
                                        'action' => 'saveUpdatedType',
                                        'prefix' => 'admin'
                                    ),
                                'method' => 'post',
                                'id' => 'edit-type',
                                'novalidate' => false
                                ));
                ?>
                    <div id="typeDataContainer">
                    
                    </div>
                    <div class="form-group">
                        <div class="clearfix">                               
                            <div class="pull-right">
                                <?php
                                    echo $this->Form->button('Close', array(
                                                'class' => 'btn btn-default',
                                                'data-dismiss' => 'modal'
                                                ));
                                    echo $this->Form->button('Save', array(
                                                'class' => 'btn btn-primary btn-grad btn-left',
                                                'type' => 'submit'
                                                ));
                                ?>
                            </div>
                        </div>
                    </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>

<script type="text/html" id="editTypeData">
    <% console.log(data); %>
	<input type='hidden' name='data[Type][id]' value="<%= data.Type.id %>" />
	<div class="form-group">
		<label class="control-label">Type Name<span>*</span></label>
    	<input type='text' class='form-control' required placeholder='Type Name' value="<%= data.Type.name %>" name='data[Type][name]' />
	</div>
</script>