<?php
App::uses('AppModel', 'Model');

class Type extends AppModel {
	public $name = 'Type';
	public $validate = array(
		'name' => array(
			'rule1' => array(
				'rule' => 'notBlank',
				'message' => 'Please enter type name.'
			),
			'isUnique' => array(
				'rule' => 'isUnique',
				'message' => 'This type is already in use.',
				'on' => 'update'
			)
		)
	);
}