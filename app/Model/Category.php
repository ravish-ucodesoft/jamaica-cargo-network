<?php
App::uses('AppModel', 'Model');
/**
 * Category Model
 *
 */
class Category extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	 public $actsAs = array('Tree');

	 public $findMethods = array('available' => true);

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'title' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Category is required',
			),
			'minLength' => array(
				'rule' => array('minLength', '3'),
				'message' => 'Category name must be at least 3 characters long.'
			),
			'isUniqueCategory' => array(
				'rule' => array('isUniqueCategory'),
				'message' => 'Please choose another Category name, given category already exists.',
			)
		)
	);

	public function isUniqueCategory() {
		$data = $this->data;

		if (isset($data['Category']['title']) && !empty($data['Category']['title'])) {

			$title = $this->find('first', array(
				'conditions' => array(
					'Category.parent_id' => $data['Category']['parent_id'],
					'Category.title' => $data['Category']['title']
				),
				'fields' => array(
					'id'
				)
			));
			
			if (!empty($title)) {
				return false;
			}
			return true;
		}

	}

}
