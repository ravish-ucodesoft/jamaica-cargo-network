<?php
class TypesController extends AppController
{
	public $helper = array('Html', 'Form');
	public $components = array('Paginator', 'RequestHandler');

	public $uses = array('Type');
	public $layout = 'backend';

	public function beforeFilter() {
		parent::beforeFilter();
	}

/**
 * Method admin_index to get all the types from database for admin view
 *
 * @return void
 */
	public function admin_index() {
		$this->loadModel('Type');
		
		$conditions = array();
		if (isset($this->request->query) && !empty($this->request->query)) {
			$searchData = array(
				'OR' => array(
					'Type.name LIKE' => '%'. $this->request->query['search'] .'%'
					)
				);
			$conditions = array_merge($conditions, $searchData);
		}
		$this->Paginator->settings = array(
			'conditions' => $conditions,
			'limit' => 2,
			'order' => 'Type.created Desc',
		);
		$types = $this->Paginator->paginate('Type');
		$this->set(compact('types'));
		if ($this->request->is('ajax')) {
			$this->layout = '';
			$this->autoRender = false;
			$this->viewPath = 'Elements' . DS . 'backend' . DS . 'Types';
			$this->render('listing');
		}
	}

/** 
 * Method admin_ajaxGetTypeData to get type data for editing
 *
 * @param $typeId int id of the type
 * @return void
 */
	public function admin_ajaxGetTypeData($typeID = null) {
		$typeID = base64_decode($typeID);
		
		$this->loadModel('Type');
		$getTypeData = $this->Type->find('first',array(
				'conditions' => array(
					'Type.id' => (int)$typeID 
				),
			)
		);

		$this->set(array('resp' => $getTypeData, '_serialize' => 'resp'));
	}

/**
 * Method admin_saveUpdatedType to save the updated record of the type form admin panel
 *
 * @return void
 */
	public function admin_saveUpdatedType() {
		$this->request->allowMethod('post','put');

		$this->loadModel('Type');
		if ($this->Type->save($this->request->data)) {
			$this->Session->setFlash(__('Type updated successfully.'), 'default', 'success');
			$this->redirect($this->referer());
		}
		$errors = $this->Type->validationErrors;
		if (!empty($errors)) {
			$errorMsg = $this->_setSaveAssociateValidationError($errors);
		}
		$this->Session->setFlash(__('Edit type request not completed due to following : <br/>' . $errorMsg . ' Try again!'), 'default', 'error');
		$this->redirect($this->referer());
	}

/**
 * Method admin_addType to add new type in admin panel
 *
 * @return void
 */
	public function admin_addType() {
		$this->request->allowMethod('post','put');

		$this->loadModel('Type');
		if ($this->Type->save($this->request->data)) {
			$this->Session->setFlash(__('Type added successfully.'), 'default', 'success');
			$this->redirect($this->referer());
		}
		$errors = $this->Type->validationErrors;
		if (!empty($errors)) {
			$errorMsg = $this->_setSaveAssociateValidationError($errors);
		}
		$this->Session->setFlash(__('Add type request not completed due to following : <br/>' . $errorMsg . ' Try again!'), 'default', 'error');
		$this->redirect($this->referer());
	}

}
