<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Controller', 'Controller');
App::uses('CrudControllerTrait', 'Crud.Lib');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

	//use CrudControllerTrait;

/**
 * List of global controller components
 *
 * @var array
 */

	
	public $components = array(
		/*'Crud.Crud' => [
			'listeners' => [
				'Crud.Api',
				'Crud.ApiPagination',
				'Crud.ApiQueryLog'
			]
		],*/
		'Auth' => array(
			'authenticate' => array(
				'Form' => array(
					'contain' => false,
					'scope' => array('User.is_deleted' => 0)
				)
			),
			'flash' => array(
				'element' => 'error',
				'key' => 'auth',
				'params' => array()
			)
		),
		'Session',
		'Cookie',
		//'DebugKit.Toolbar',
		'Paginator' => ['settings' => ['paramType' => 'querystring', 'limit' => 30]]
	);
	

	public function beforeFilter() {
		$this->_checkAdminPrefix();
	}

	/**
 * Method checkAdminPrefix to control auth for front end and admin user
 *
 * @return void
 */
	protected function _checkAdminPrefix() {

		if (!$this->Auth->loggedIn()) {
			$this->Auth->authError = false;
		}

		$this->Auth->allow('admin_login','admin_subAdminActivateAccount', 'admin_forgotPassword', 'admin_resetPassword');
		if (isset($this->request->params['prefix']) && $this->request->params['prefix'] == 'admin') {
			$this->Auth->loginAction = array(
				'controller' => 'admins',
				'action' => 'admin_login',
				'admin' => true
		    );
		    $this->Auth->logoutRedirect = array(
				'controller' => 'admins',
				'action' => 'admin_login',
				'admin' => true
		    );
		    $this->Auth->loginRedirect = array(
				'controller' => 'admins',
				'action' => 'admin_index',
				'admin' => true
		    );

		    if ($this->Session->check('Auth')) {
		    	$getAdminUserName = $this->_getUserProfileData();
				$this->set('getAdminUserName', $getAdminUserName);
         	}

		} else {
			$this->Auth->loginAction = array(
			'controller' => 'homes',
			'action' => 'index'
		    );
		    $this->Auth->logoutRedirect = array(
			'controller' => 'homes',
			'action' => 'index'
		    );
		    $this->Auth->loginRedirect = array(
			'controller' => 'users',
			'action' => 'dashboard'
		    );
		}
	}

/**
 * Method _checkUserType checks if admin user is logging in at frontend or vice versa
 *
 * @return string url
 */
	protected function _checkUserType($userTypes = array()){
		if (in_array($this->Auth->user('user_type_id'), $userTypes)) {
		    return true;
		}
		return false;
	}

/**
 * Method __redirectAfterLogin to check user is admin or subadmin after logged in
 *
 * @return string url
 */
    protected function _redirectAfterLogin(){
    	if (
    		in_array(
    			$this->Session->read('Auth.User.user_type_id'),
    			array(
    					Configure::read('UserTypes.Admin'),
    					Configure::read('UserTypes.SubAdmin')
    				)
    			)
    		) {
            return $this->redirect($this->Auth->redirectUrl());
        }
		return $this->redirect(array('controller' => 'admins', 'action' => 'dashboard', 'admin' => true));
    }

/**
 * Method _getAdminUserName to get admin user first name to show in header bar
 *
 * @return $adminUserName array array of first name of admin user name
 */
    protected function _getUserProfileData() {
        $this->loadModel('User');
        $adminUserName = $this->User->find('first',
            array(
                'conditions' => array(
                    'User.id' => $this->Session->read('Auth.User.id')
                ),
                'recursive' => -1
                )
            );
        return $adminUserName;
    }

/**
 * Method: checkAdminType to check the logged in user is admin or sub-admin.
 * it also check that while redirecting user to add space page, if user is logged in or not.
 *
 * @return bool
 */
	protected function _checkIfAdminOrUser(){
        if ($this->Session->read('Auth.User.user_type_id') == configure::read('UserTypes.Admin')) {
                    CakeLog::write('debug',print_r('Inside checkIfAdminOrUser::User is ADMIN',true));
		    return true;
		}

		if ($this->Session->check('Auth.User') && $this->Session->read('Auth.User.user_type_id') == configure::read('UserTypes.User')) {
                    CakeLog::write('debug',print_r('Inside checkIfAdminOrUser::User is Normal User',true));
			return true;
		}

		return false;
	}

/**
 * Method _changeAccountStatus common function to change status
 *
 * @param $recArray array of record
 * @param $mdoel string name of the model
 * @return bool
 */
    protected function _changeAccountStatus($recArray = array(), $model =null) {
        $this->loadModel($model);
        $status = $recArray[$model]['is_activated'] == (int)false ? (int)true : (int)false;
        $this->$model->updateAll(
                array(
                    $model.'.is_activated' => $status
                    ),
                array(
                    $model.'.id' => $recArray[$model]['id']
                    )
                );

        return $status;
    }

/**
 * Method _deleteAccount common function to delete user
 *
 * @param $id int ID of the user in table User.
 * @return bool
 */
	protected function _deleteAccount($id = null, $deleteCheck = null, $model = null) {
		$deleteFlag = $deleteCheck == (int)false ? (int)true : (int)false;
		$statusFlag = $deleteFlag == (int)false ? (int)true : (int)false;
		$this->loadModel($model);
      	$this->$model->updateAll(
      			array(
                    $model.'.is_deleted' => $deleteFlag,
                    $model.'.is_activated' => $statusFlag
                    ),
               array(
                    $model.'.id' => $id
                    )
               );
		return $deleteFlag;
	}

/**
 * Method _changePassword Common function to change password functionality for admin and front end users
 *
 * @return void
 */
    protected function _changePassword(){
    	
        $this->loadModel('User');
        $this->User->unvalidate(array('username', 'email'));
        if ($this->User->save($this->request->data)) {
            $this->Session->setFlash(__('Password has been changed successfully.'), 'default', 'success');
        } else {
            $errors = $this->User->validationErrors;
            if (!empty($errors)) {
                $errorMsg = $this->_setValidationError($errors);
            }
            $this->Session->setFlash(__('Password change request not completed due to following error : <br/>' . $errorMsg . ' Try again!'), 'default', 'error');
        }
        
        $this->redirect($this->referer());
    }
    
/**
 * Method _setValidationError to make cakephp validation errors when save associate query runs for save user profile
 *
 * @param $errors array the array of errors
 * @return $str the string of errors
 */
    protected function _setSaveAssociateValidationError($errors = array()) {

        $str = null;
        $hasErrors = Hash::extract($errors, '{s}.{n}');
        if (!empty($hasErrors)) {
            $str = '<ul>';
            foreach ($hasErrors as $key => $val):
                $str .= '<li>'.$val.'</li>';
            endforeach;
            $str .= '</ul>';
        }
        return $str;
    }

/**
 * Method _forgotPassword Common function to forgot password functionality for admin and front end users
 *
 * @return void
 */
	protected function _forgotPassword($userData){
		$this->loadModel('User');
		
		if ($userData['User']['is_activated'] == (int)false) {
			$this->Session->setFlash(__('Your account is blocked by administrator.'), 'default', 'error');
			$this->__manageRedirection();
		}
			
		if ($this->_sendForgotPasswordEmail($userData)) {
			$this->Session->setFlash(__('Please check your mailbox, An instruction has been sent to reset your password.'), 'default', 'success');
		} else {
			$this->Session->setFlash(__('Some error occurred. Please try again'), 'default', 'error');
		}
		$this->__manageRedirection();
	}

/**
 * Method: send_email_message common function to send emails
 *
 * @param $to string the user email to whom the email will be sent
 * @param $email_body the body of the email
 * @param $subject the subject of the email
 */
	public function _sendEmailMessage($to = null, $email_body = null, $subject = null, $bcc = null, $cc = null)
    { 
		App::uses('CakeEmail', 'Network/Email');
      	$email = new CakeEmail('smtp');
      	$email->emailFormat('both');
      	$email->from("support@renacy.com");
      	$email->sender("support@renacy.com");
      	$email->to($to);
      	$email->subject($subject);

      	if ($email->send($email_body)) {
			return true;
		}
		return false;
    }

/**
 * Method __manageRedirection to manage redirection urls for admin and front end users
 *
 * @return void
 */
	protected function __manageRedirection() {
		if (!isset($this->request->params['prefix'])) {
			$this->redirect('/?forgotpassword=1');
		}
		$this->redirect($this->referer());
	}
    

}
