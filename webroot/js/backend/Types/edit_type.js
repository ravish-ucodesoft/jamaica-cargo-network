$(function () {
    var editTypeData = function(url) {
        $.ajax({
            url: url,
            dataType: "json",
            success:function( data ) {
                    console.log(data);
            		var popupTemplate = $("#editTypeData").html();
						resultData = _.template(popupTemplate, {data: data});
                  $('#typeDataContainer').html(resultData);
            }
        });
    };
	var editTypeValidate = function(ele){
		$(ele).validate({
			rules: {
                "data[Type][name]": {
				    required: true
                }
			}
		});
	};
	$(document).ready(function(){
        $(document).on('click', '.edit_type_data', function(){
            var url = $(this).attr('data-url');
            editTypeData(url);
        });
		editTypeValidate('#edit-type');
	});

});
