$(function () {
    var addTypeValidate = function(ele){
		$(ele).validate({
			rules: {
                "data[Type][name]": {
                    required: true
                }
			}
		});
	};
	$(document).ready(function(){
		addTypeValidate('#add-type');
	});

});
