$(function () {
        var addServiceTaxChargesValidate = function(ele){
        $(ele).validate({
            rules: {
                "data[ServiceTaxCharges][commission_percentage]": {
                    number: true,
                    validateCommissionPercentage: true
                },
                "data[ServiceTaxCharges][service_tax_percentage]": {
                    number: true,
                    validateTaxPercentage: true
                }
            },
            messages: {
                "data[ServiceTaxCharges][commission_percentage]": {
                    required: 'Only number is allowed.'
                },
                "data[ServiceTaxCharges][service_tax_percentage]": {
                    required: 'Only number is allowed.'
                }
            }
        });
        jQuery.validator.addMethod("validateTaxPercentage", function() {
            if($('input[name="data[ServiceTaxCharges][service_tax_percentage]"]').val() >= 0 && 
                $('input[name="data[ServiceTaxCharges][service_tax_percentage]"]').val() <= 100) {
                return true;
            }
            return false;
        }, "Percentage Should be between 0 to 100.");
        jQuery.validator.addMethod("validateCommissionPercentage", function() {
            if($('input[name="data[ServiceTaxCharges][commission_percentage]"]').val() >= 0 && 
                $('input[name="data[ServiceTaxCharges][commission_percentage]"]').val() <= 100) {
                return true;
            }
            return false;
        }, "Percentage Should be between 0 to 100.");
    };

    var confirmPopup = function(space_id) {
        $('#serviceConfirmModal').modal();
    };

	$(document).ready(function(){
        addServiceTaxChargesValidate('#add-service-rax-charges');

        $("input[name='data[ServiceTaxCharges][commission_paid_by]']").change(function() {
            confirmPopup();
        });

        $(".no-service").click(function(event) {
            event.preventDefault();
            if($("#ServiceTaxChargesCommissionPaidBy2").prop("checked")) {
                $("#ServiceTaxChargesCommissionPaidBy2").removeAttr("checked");
                $("#ServiceTaxChargesCommissionPaidBy1").prop("checked",true);
            } else {
                $("#ServiceTaxChargesCommissionPaidBy1").removeAttr("checked");
                $("#ServiceTaxChargesCommissionPaidBy2").prop("checked",true);
            }
            
        });
    });
});